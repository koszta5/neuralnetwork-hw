'''
Created on Mar 21, 2012

@author: Pavel Kostelnik
'''
from pybrain.datasets import SupervisedDataSet
import os
class DataLoader(object):
    '''
    classdocs
    '''
    def loadData(self):
        f = open(".."+os.sep+"data"+os.sep+self.pathToSource, "r")
        line = f.readline()
        while line:
            line = line.rstrip()
            samples = line.split(",")
            self.dataSet.addSample((float(samples[0]), float(samples[1]), float(samples[2])), (float(samples[3]),))
            line = f.readline()
            
    
        
            

    def __init__(self, pathToSource):
        '''
        Constructor
        '''
        self.dataSet = SupervisedDataSet(3,1)
        self.pathToSource = pathToSource