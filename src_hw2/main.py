'''
Created on Mar 21, 2012

@author: Pavel Kostelnik
'''
import os,sys

from dataLoader import DataLoader
from network import Network


class Main(object):
    '''
    classdocs
    '''
    def main(self):
        sourceFile = self.chooseData()
        self.dataLoader = DataLoader(sourceFile)
        self.dataLoader.loadData()
        
        self.network = Network(self.dataLoader)
        self.network.train()
        self.network.crossValidate()
    
    def chooseData(self):
        filenames = os.listdir(".."+os.sep+"data")
        print "choose file to be data source (choose number)"
        for i in range(0,len(filenames)):
            print "to choose "+filenames[i]+" press "+str(i)+" and enter" 
        number = sys.stdin.readline()
        return filenames[int(number)]
    

    def __init__(self):
        '''
        Constructor
        '''
        pass
    
if __name__ == "__main__":
    app = Main()
    app.main()
