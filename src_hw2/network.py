'''
Created on Mar 21, 2012

@author: Pavel Kostelnik
'''
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.validation import CrossValidator
from pybrain.supervised.trainers.backprop import BackpropTrainer
class Network(object):
    '''
    classdocs
    '''
    def train(self):
        self.trainer.trainUntilConvergence(maxEpochs=70, validationProportion=0.25,continueEpochs=3)
        self.trainer.testOnData(verbose=True)

    def crossValidate(self):
        print "\nCrossvalidation results for "+self.dataLoaderObj.pathToSource
        print "======================================================="
        self.crossValidator = CrossValidator(self.trainer, self.dataLoaderObj.dataSet, n_folds=10)
        self.crossValidator.validate()
        
    def __init__(self, dataLoader):
        '''
        Constructor
        '''
        self.dataLoaderObj = dataLoader
        self.net = buildNetwork(3,5,1, bias=True)
        self.trainer = BackpropTrainer(self.net, self.dataLoaderObj.dataSet, learningrate=0.01,momentum=0.4,verbose=True)
        
        